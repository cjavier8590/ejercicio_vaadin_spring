package com.example.demo.Visual;

import com.example.demo.dominio.*;
import com.vaadin.event.selection.SelectionEvent;
import com.vaadin.event.selection.SelectionListener;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;

import javax.swing.*;
import java.util.Optional;
import java.util.Set;

/**
 * Created by Javier Lopez on 15/07/2017.
 */
@SpringUI
public class App extends UI {

    @Autowired
    ColegioRepository colegio;

    @Autowired
    GradoRepository gra;
    Optional<Colegio> elegido;

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        VerticalLayout principal = new VerticalLayout();

        VerticalLayout vertical2 = new VerticalLayout();
        VerticalLayout vertical = new VerticalLayout();

        vertical.setWidth("45%");
        vertical2.setWidth("45%");
        HorizontalLayout primero = new HorizontalLayout();
        HorizontalLayout segundo = new HorizontalLayout();

        TextField nombreColegio = new TextField();
        nombreColegio.setCaption("Nombre del Colegio");

        TextField direccionColegio = new TextField();
        direccionColegio.setCaption("Dirección del Colegio");

        TextField telefonoColegio = new TextField();
        telefonoColegio.setCaption("Teléfono del Colegio");

        Grid<Colegio> gridCole = new Grid<>();
        gridCole.addColumn(Colegio::getId).setCaption("ID");
        gridCole.addColumn(Colegio::getNombreColegio).setCaption("Nombre Colegio");
        gridCole.addColumn(Colegio::getDireccionColegio).setCaption("Direccion Colegio");
        gridCole.addColumn(Colegio::getTelefonoColegio).setCaption("Telefono Colegio");

        Button guardaColegio = new Button();
        guardaColegio.setCaption("Agregar Colegio");
        guardaColegio.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                Colegio cole = new Colegio();
                cole.setNombreColegio(nombreColegio.getValue());
                cole.setDireccionColegio(direccionColegio.getValue());
                cole.setTelefonoColegio(telefonoColegio.getValue());
                colegio.save(cole);
                gridCole.setItems(colegio.findAll());
                Notification.show("Colegio agregado exitosamente");
            }
        });

        gridCole.addSelectionListener(event -> {
            elegido = event.getFirstSelectedItem();

            Notification.show(elegido.get().getNombreColegio() + " ver mas abajo");
            segundo.setVisible(true);
        });




        TextField nombreGrado = new TextField();
        nombreGrado.setCaption("Nombre Grado");

        Button guardaGrado = new Button();
        guardaGrado.setCaption("Agregar Grado");

        Grid<Grado> gridG = new Grid<>();
        gridG.addColumn(Grado::getId).setCaption("ID");
        gridG.addColumn(Grado::getNombreGrado).setCaption("Nombre Grado");
        //gridG.addColumn(Grado::getColegio).setCaption("Colegio");

        guardaGrado.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                Grado grade = new Grado();
                Colegio c;
                grade.setColegio(colegio.getOne(elegido.get().getId()));
                //Notification.show(elegido.get().getTelefonoColegio());
                grade.setNombreGrado(nombreGrado.getValue());
                gra.save(grade);

                gridG.setItems(gra.findAll());

            }
        });

        gridG.addSelectionListener(new SelectionListener<Grado>() {
            @Override
            public void selectionChange(SelectionEvent<Grado> selectionEvent) {
                Optional<Grado> gradoElegido = selectionEvent.getFirstSelectedItem();
                Notification.show("El grado seleccionado pertenece al colegio " + gradoElegido.get().getColegio().getNombreColegio());
            }
        });

        primero.addComponents(nombreColegio, direccionColegio, telefonoColegio);
        segundo.addComponents(nombreGrado);

        vertical.addComponents(primero, gridCole, guardaColegio);
        vertical2.addComponents(segundo, gridG, guardaGrado);
        principal.addComponents(vertical, vertical2);

        principal.setComponentAlignment(vertical2, Alignment.TOP_RIGHT);

        principal.setComponentAlignment(vertical, Alignment.TOP_LEFT);


        setContent(principal);
    }
}
