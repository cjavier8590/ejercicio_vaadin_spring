package com.example.demo.dominio;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Javier Lopez on 16/07/2017.
 */
public interface ColegioRepository extends JpaRepository<Colegio, Long> {

}
