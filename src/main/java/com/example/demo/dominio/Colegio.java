package com.example.demo.dominio;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Javier Lopez on 16/07/2017.
 */
@Entity
@Table(name = "colegio")
public class Colegio {
    private String nombreColegio;
    private String direccionColegio;
    private String telefonoColegio;
    @OneToMany(mappedBy = "colegio", cascade = CascadeType.ALL)
    private Set<Grado> grado;



    public Set<Grado> getGrado() {
        return grado;
    }

    public void setGrado(Set<Grado> grado) {
        this.grado = grado;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Colegio(String nombreColegio, String direccionColegio, String telefonoColegio) {
        this.nombreColegio = nombreColegio;
        this.direccionColegio = direccionColegio;
        this.telefonoColegio = telefonoColegio;
    }

    public Colegio() {
    }

    public String getNombreColegio() {
        return nombreColegio;
    }

    public void setNombreColegio(String nombreColegio) {
        this.nombreColegio = nombreColegio;
    }

    public String getDireccionColegio() {
        return direccionColegio;
    }

    public void setDireccionColegio(String direccionColegio) {
        this.direccionColegio = direccionColegio;
    }

    public String getTelefonoColegio() {
        return telefonoColegio;
    }

    public void setTelefonoColegio(String telefonoColegio) {
        this.telefonoColegio = telefonoColegio;
    }
}
