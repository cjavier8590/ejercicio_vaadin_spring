package com.example.demo.dominio;

import javax.persistence.*;

/**
 * Created by Javier Lopez on 16/07/2017.
 */
@Entity
public class Grado {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nombreGrado;
    @ManyToOne
    private Colegio colegio;

    public Grado(String nombreGrado, Colegio colegio) {
        this.nombreGrado = nombreGrado;
        this.colegio = colegio;
    }

    public Grado() {
    }


    @JoinColumn(
            name = "colegio_id"

    )
    public Colegio getColegio() {
        return colegio;
    }

    public void setColegio(Colegio colegio) {
        this.colegio = colegio;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreGrado() {
        return nombreGrado;
    }

    public void setNombreGrado(String nombreGrado) {
        this.nombreGrado = nombreGrado;
    }


}
